#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include <jni.h>
#include <cstring>
#include <cstdlib>
#include <sys/mman.h>
#include <array>
#include <thread>
#include <vector>
#include <utility>
#include <string>
#include <android-base/logging.h>
#include "native_hook.h"
#include "logging.h"
#include "config.h"
#include "edxp_context.h"
#include "../include/riru_new.h"
#include <map>
#include <xhook.h>
#include "main.h"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-value"

#define EXPORT extern "C" __attribute__((visibility("default")))
static int sdkLevel;
static int previewSdkLevel;
static char androidVersionName[PROP_VALUE_MAX + 1];
static  int riru_api_version;
namespace edxp {

    EXPORT void onModuleLoaded() {
        LOG(INFO) << "onModuleLoaded: welcome to EdXposed!";
        InstallInlineHooks();
    }

    EXPORT int shouldSkipUid(int uid) {
        return 0;
    }


    EXPORT void nativeForkAndSpecializePre(JNIEnv *env, jclass clazz, jint *_uid, jint *gid,
                                           jintArray *gids, jint *runtime_flags,
                                           jobjectArray *rlimits, jint *mount_external,
                                           jstring *se_info, jstring *nice_name,
                                           jintArray *fds_to_close, jintArray *fds_to_ignore,
                                           jboolean *start_child_zygote, jstring *instruction_set,
                                           jstring *app_data_dir,
            /* parameters added in Android Q */
                                           jstring *package_name, jobjectArray *packages_for_uid,
                                           jstring *sandbox_id) {
        Context::GetInstance()->OnNativeForkAndSpecializePre(env, clazz, *_uid, *gid, *gids,
                                                             *runtime_flags, *rlimits,
                                                             *mount_external, *se_info, *nice_name,
                                                             *fds_to_close,
                                                             *fds_to_ignore,
                                                             *start_child_zygote, *instruction_set,
                                                             *app_data_dir);
    }

    EXPORT int nativeForkAndSpecializePost(JNIEnv *env, jclass clazz, jint res) {
        return Context::GetInstance()->OnNativeForkAndSpecializePost(env, clazz, res);
    }

    EXPORT void nativeForkSystemServerPre(JNIEnv *env, jclass clazz, uid_t *uid, gid_t *gid,
                                          jintArray *gids, jint *runtime_flags,
                                          jobjectArray *rlimits, jlong *permitted_capabilities,
                                          jlong *effective_capabilities) {
        Context::GetInstance()->OnNativeForkSystemServerPre(env, clazz, *uid, *gid, *gids,
                                                            *runtime_flags, *rlimits,
                                                            *permitted_capabilities,
                                                            *effective_capabilities
        );
    }

    EXPORT int nativeForkSystemServerPost(JNIEnv *env, jclass clazz, jint res) {
        return Context::GetInstance()->OnNativeForkSystemServerPost(env, clazz, res);
    }

    /* method added in Android Q */
    EXPORT void specializeAppProcessPre(JNIEnv *env, jclass clazz, jint *uid, jint *gid,
                                        jintArray *gids, jint *runtime_flags, jobjectArray *rlimits,
                                        jint *mount_external, jstring *se_info, jstring *nice_name,
                                        jboolean *start_child_zygote, jstring *instruction_set,
                                        jstring *app_data_dir, jstring *package_name,
                                        jobjectArray *packages_for_uid, jstring *sandbox_id) {
        Context::GetInstance()->OnNativeForkAndSpecializePre(env, clazz, *uid, *gid, *gids,
                                                             *runtime_flags, *rlimits,
                                                             *mount_external, *se_info, *nice_name,
                                                             nullptr, nullptr,
                                                             *start_child_zygote, *instruction_set,
                                                             *app_data_dir);
    }
    static void new_specializeAppProcessPre(
            JNIEnv *env, jclass clazz, jint *_uid, jint *gid, jintArray *gids, jint *runtimeFlags,
            jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
            jboolean *startChildZygote, jstring *instructionSet, jstring *appDataDir,
            jboolean *isTopApp, jobjectArray *pkgDataInfoList, jobjectArray *whitelistedDataInfoList,
            jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs) {
        // added from Android 10, but disabled at least in Google Pixel devices
        Context::GetInstance()->OnNativeForkAndSpecializePre(env, clazz, *_uid, *gid, *gids,
                                                                   *runtimeFlags, *rlimits,
                                                                   *mountExternal, *seInfo, *niceName,
                                                                   nullptr, nullptr,
                                                                   *startChildZygote, *instructionSet,
                                                                   *appDataDir);
    }

    EXPORT int specializeAppProcessPost(JNIEnv *env, jclass clazz) {
        return Context::GetInstance()->OnNativeForkAndSpecializePost(env, clazz, 0);
    }
    EXPORT void new_forkAndSpecializePre(
            JNIEnv *env, jclass clazz, jint *_uid, jint *gid, jintArray *gids, jint *runtimeFlags,
            jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
            jintArray *fdsToClose, jintArray *fdsToIgnore, jboolean *is_child_zygote,
            jstring *instructionSet, jstring *appDataDir, jboolean *isTopApp, jobjectArray *pkgDataInfoList,
            jobjectArray *whitelistedDataInfoList, jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs) {
        edxp::Context::GetInstance()->OnNativeForkAndSpecializePre(env, clazz, *_uid, *gid, *gids,
                                                                   *runtimeFlags, *rlimits,
                                                                   *mountExternal, *seInfo, *niceName,
                                                                   nullptr, nullptr,
                                                                   *is_child_zygote, *instructionSet,
                                                                   *appDataDir);
    }
    static int new_forkAndSpecializePost(JNIEnv *env, jclass clazz, jint res) {
        return edxp::Context::GetInstance()->OnNativeForkAndSpecializePost(env, clazz, res);
    }
    static void new_forkSystemServerPre(
            JNIEnv *env, jclass clazz, uid_t *uid, gid_t *gid, jintArray *gids, jint *runtimeFlags,
            jobjectArray *rlimits, jlong *permittedCapabilities, jlong *effectiveCapabilities) {
        edxp::Context::GetInstance()->OnNativeForkSystemServerPre(env, clazz, *uid, *gid, *gids,
                                                                  *runtimeFlags, *rlimits,
                                                                  *permittedCapabilities,
                                                                  *effectiveCapabilities
        );
    }
    static int new_forkSystemServerPost(JNIEnv *env, jclass clazz, jint res) {
        return edxp::Context::GetInstance()->OnNativeForkSystemServerPost(env, clazz, res);
    }

    EXPORT void *init(void *arg) {
        static int step = 0;
        step += 1;
        LOGE("SandHook: HookEntry class . %d", getpid());
        static void *_module;

        switch (step) {
            case 1: {
                auto core_max_api_version = *(int *) arg;
                riru_api_version = core_max_api_version;
                return &riru_api_version;
            }
            case 2: {
                switch (riru_api_version) {
                    case 9: {
                        auto module = (RiruModuleInfoV9 *) malloc(sizeof(RiruModuleInfoV9));
                        memset(module, 0, sizeof(RiruModuleInfoV9));
                        _module = module;

                        module->supportHide = true;

                        module->version = riru_api_version;
                        module->versionName = "22";
                        module->onModuleLoaded = onModuleLoaded;
                        module->shouldSkipUid = shouldSkipUid;
                        module->forkAndSpecializePre = new_forkAndSpecializePre;
                        module->forkAndSpecializePost = new_forkAndSpecializePost;
                        module->specializeAppProcessPre = new_specializeAppProcessPre;
                        module->specializeAppProcessPost = specializeAppProcessPost;
                        module->forkSystemServerPre = new_forkSystemServerPre;
                        module->forkSystemServerPost = new_forkSystemServerPost;
                        return module;
                    }
                }
            }
            case 3: {
                free(_module);
                return nullptr;
            }
            default:
                return nullptr;
        }
    }

}



using nativeForkAndSpecialize_r_t = jint(
        JNIEnv *, jclass, jint, jint, jintArray, jint, jobjectArray, jint, jstring, jstring,
        jintArray, jintArray, jboolean, jstring, jstring, jboolean, jobjectArray, jobjectArray,
        jboolean, jboolean);



void put_native_method(const char *className, const JNINativeMethod *methods, int numMethods) {
    (*native_methods)[className] = new JNINativeMethodHolder(methods, numMethods);
}
void set_nativeForkAndSpecialize(void *addr) {
    _nativeForkAndSpecialize = addr;
}


void set_nativeSpecializeAppProcess(void *addr) {
    _nativeSpecializeAppProcess = addr;
}

void set_nativeForkSystemServer(void *addr) {
    _nativeForkSystemServer = addr;
}

void set_SystemProperties_set(void *addr) {
    _SystemProperties_set = addr;
}
void SystemProperties_set(JNIEnv *env, jobject clazz, jstring keyJ, jstring valJ) {
    const char *key = env->GetStringUTFChars(keyJ, JNI_FALSE);
    char user[16];
    int no_throw = sscanf(key, "sys.user.%[^.].ce_available", user) == 1;
    env->ReleaseStringUTFChars(keyJ, key);

    ((SystemProperties_set_t *) _SystemProperties_set)(env, clazz, keyJ, valJ);

    jthrowable exception = env->ExceptionOccurred();
    if (exception && no_throw) {
        LOGW("prevented data destroy");

        env->ExceptionDescribe();
        env->ExceptionClear();
    }
}
static int shouldSkipUid(int uid) {
    int appId = uid % 100000;

    // limit only regular app, or strange situation will happen, such as zygote process not start (dead for no reason and leave no clues?)
    // https://android.googlesource.com/platform/frameworks/base/+/android-9.0.0_r8/core/java/android/os/UserHandle.java#151
    if (appId >= 10000 && appId <= 19999) return 0;
    return 1;
}
static int replace_forkAndSpecializePost(JNIEnv *env, jclass clazz, jint res) {
    return edxp::Context::GetInstance()->OnNativeForkAndSpecializePost(env, clazz, res);
}
static void replace_specializeAppProcessPre(
        JNIEnv *env, jclass clazz, jint *_uid, jint *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
        jboolean *startChildZygote, jstring *instructionSet, jstring *appDataDir,
        jboolean *isTopApp, jobjectArray *pkgDataInfoList, jobjectArray *whitelistedDataInfoList,
        jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs) {
    // added from Android 10, but disabled at least in Google Pixel devices
    edxp::Context::GetInstance()->OnNativeForkAndSpecializePre(env, clazz, *_uid, *gid, *gids,
                                                         *runtimeFlags, *rlimits,
                                                         *mountExternal, *seInfo, *niceName,
                                                         nullptr, nullptr,
                                                         *startChildZygote, *instructionSet,
                                                         *appDataDir);
}

static void replace_forkAndSpecializePre(
        JNIEnv *env, jclass clazz, jint *_uid, jint *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
        jintArray *fdsToClose, jintArray *fdsToIgnore, jboolean *is_child_zygote,
        jstring *instructionSet, jstring *appDataDir, jboolean *isTopApp, jobjectArray *pkgDataInfoList,
        jobjectArray *whitelistedDataInfoList, jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs) {
    edxp::Context::GetInstance()->OnNativeForkAndSpecializePre(env, clazz, *_uid, *gid, *gids,
                                                               *runtimeFlags, *rlimits,
                                                               *mountExternal, *seInfo, *niceName,
                                                               nullptr, nullptr,
                                                               *is_child_zygote, *instructionSet,
                                                               *appDataDir);
}
static int replace_specializeAppProcessPost(JNIEnv *env, jclass clazz) {
    return edxp::Context::GetInstance()->OnNativeForkAndSpecializePost(env, clazz, 0);
}
static void replace_forkSystemServerPre(
        JNIEnv *env, jclass clazz, uid_t *uid, gid_t *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jlong *permittedCapabilities, jlong *effectiveCapabilities) {
    edxp::Context::GetInstance()->OnNativeForkSystemServerPre(env, clazz, *uid, *gid, *gids,
                                                              *runtimeFlags, *rlimits,
                                                              *permittedCapabilities,
                                                              *effectiveCapabilities
    );
}
static int replace_forkSystemServerPost(JNIEnv *env, jclass clazz, jint res) {
    return edxp::Context::GetInstance()->OnNativeForkSystemServerPost(env, clazz, res);
}

static void nativeForkAndSpecialize_pre(
        JNIEnv *env, jclass clazz, jint &uid, jint &gid, jintArray &gids, jint &runtime_flags,
        jobjectArray &rlimits, jint &mount_external, jstring &se_info, jstring &se_name,
        jintArray &fdsToClose, jintArray &fdsToIgnore, jboolean &is_child_zygote,
        jstring &instructionSet, jstring &appDataDir, jboolean &isTopApp, jobjectArray &pkgDataInfoList,
        jobjectArray &whitelistedDataInfoList, jboolean &bindMountAppDataDirs, jboolean &bindMountAppStorageDirs) {
    if ( shouldSkipUid(uid))
        return;
    replace_forkAndSpecializePre(
            env, clazz, &uid, &gid, &gids, &runtime_flags, &rlimits, &mount_external,
            &se_info, &se_name, &fdsToClose, &fdsToIgnore, &is_child_zygote,
            &instructionSet, &appDataDir, &isTopApp, &pkgDataInfoList, &whitelistedDataInfoList,
            &bindMountAppDataDirs, &bindMountAppStorageDirs);
}
static void nativeForkAndSpecialize_post(JNIEnv *env, jclass clazz, jint uid, jint res) {

    if (res == 0) unhook_jniRegisterNativeMethods();
    if ( shouldSkipUid(uid))
        return;
    replace_forkAndSpecializePost(env, clazz, res);
}
static void nativeSpecializeAppProcess_pre(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jstring niceName,
        jboolean startChildZygote, jstring instructionSet, jstring appDataDir,
        jboolean &isTopApp, jobjectArray &pkgDataInfoList, jobjectArray &whitelistedDataInfoList,
        jboolean &bindMountAppDataDirs, jboolean &bindMountAppStorageDirs) {

    replace_specializeAppProcessPre(
            env, clazz, &uid, &gid, &gids, &runtimeFlags, &rlimits, &mountExternal, &seInfo,
            &niceName, &startChildZygote, &instructionSet, &appDataDir, &isTopApp,
            &pkgDataInfoList, &whitelistedDataInfoList, &bindMountAppDataDirs, &bindMountAppStorageDirs);

}

static void nativeSpecializeAppProcess_post(JNIEnv *env, jclass clazz) {

    unhook_jniRegisterNativeMethods();
    replace_specializeAppProcessPost(env, clazz);

}

static void nativeForkSystemServer_pre(
        JNIEnv *env, jclass clazz, uid_t &uid, gid_t &gid, jintArray &gids, jint &debug_flags,
        jobjectArray &rlimits, jlong &permittedCapabilities, jlong &effectiveCapabilities) {
    replace_forkSystemServerPre(
            env, clazz, &uid, &gid, &gids, &debug_flags, &rlimits, &permittedCapabilities,
            &effectiveCapabilities);

}
static void nativeForkSystemServer_post(JNIEnv *env, jclass clazz, jint res) {
    replace_forkSystemServerPost(env, clazz, res);
}

jint nativeForkAndSpecialize_r(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtime_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jintArray fdsToIgnore, jboolean is_child_zygote,
        jstring instructionSet, jstring appDataDir, jboolean isTopApp, jobjectArray pkgDataInfoList,
        jobjectArray whitelistedDataInfoList, jboolean bindMountAppDataDirs, jboolean bindMountAppStorageDirs) {

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_r_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, fdsToIgnore, is_child_zygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}

jint nativeForkSystemServer_samsung_q(
        JNIEnv *env, jclass cls, uid_t uid, gid_t gid, jintArray gids, jint runtimeFlags,
        jint space, jint accessInfo, jobjectArray rlimits, jlong permittedCapabilities,
        jlong effectiveCapabilities) {

    nativeForkSystemServer_pre(
            env, cls, uid, gid, gids, runtimeFlags, rlimits, permittedCapabilities,
            effectiveCapabilities);

    jint res = ((nativeForkSystemServer_samsung_q_t *) _nativeForkSystemServer)(
            env, cls, uid, gid, gids, runtimeFlags, space, accessInfo, rlimits, permittedCapabilities,
            effectiveCapabilities);

    nativeForkSystemServer_post(env, cls, res);
    return res;
}
jint nativeForkAndSpecialize_p(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtime_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jintArray fdsToIgnore, jboolean is_child_zygote,
        jstring instructionSet, jstring appDataDir) {

    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_p_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, fdsToIgnore, is_child_zygote, instructionSet, appDataDir);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}
jint nativeForkAndSpecialize_oreo(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint debug_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jintArray fdsToIgnore, jstring instructionSet, jstring appDataDir) {

    jboolean is_child_zygote = JNI_FALSE;
    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_oreo_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, fdsToIgnore, instructionSet, appDataDir);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}
jint nativeForkAndSpecialize_marshmallow(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint debug_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jstring instructionSet, jstring appDataDir) {

    jintArray fdsToIgnore = nullptr;
    jboolean is_child_zygote = JNI_FALSE;
    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_marshmallow_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, instructionSet, appDataDir);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}
jint nativeForkAndSpecialize_r_dp3(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtime_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jintArray fdsToIgnore, jboolean is_child_zygote,
        jstring instructionSet, jstring appDataDir, jboolean isTopApp, jobjectArray pkgDataInfoList,
        jboolean bindMountAppStorageDirs) {

    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_r_dp3_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, fdsToIgnore, is_child_zygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            bindMountAppStorageDirs);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}
jint nativeForkAndSpecialize_r_dp2(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtime_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jintArray fdsToIgnore, jboolean is_child_zygote,
        jstring instructionSet, jstring appDataDir, jboolean isTopApp, jobjectArray pkgDataInfoList) {

    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_r_dp2_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, fdsToIgnore, is_child_zygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}
jint nativeForkAndSpecialize_samsung_p(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtime_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jint category, jint accessInfo,
        jstring se_name, jintArray fdsToClose, jintArray fdsToIgnore, jboolean is_child_zygote,
        jstring instructionSet, jstring appDataDir) {

    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_samsung_p_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external, se_info, category,
            accessInfo, se_name, fdsToClose, fdsToIgnore, is_child_zygote, instructionSet,
            appDataDir);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}

jint nativeForkAndSpecialize_samsung_o(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint debug_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jint category, jint accessInfo,
        jstring se_name, jintArray fdsToClose, jintArray fdsToIgnore, jstring instructionSet,
        jstring appDataDir) {

    jboolean is_child_zygote = JNI_FALSE;
    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_samsung_o_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external, se_info, category,
            accessInfo, se_name, fdsToClose, fdsToIgnore, instructionSet, appDataDir);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}

jint nativeForkAndSpecialize_samsung_n(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint debug_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jint category, jint accessInfo,
        jstring se_name, jintArray fdsToClose, jstring instructionSet, jstring appDataDir,
        jint a1) {

    jintArray fdsToIgnore = nullptr;
    jboolean is_child_zygote = JNI_FALSE;
    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_samsung_n_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external, se_info, category,
            accessInfo, se_name, fdsToClose, instructionSet, appDataDir, a1);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}

jint nativeForkAndSpecialize_samsung_m(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint debug_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jint category, jint accessInfo,
        jstring se_name, jintArray fdsToClose, jstring instructionSet, jstring appDataDir) {

    jintArray fdsToIgnore = nullptr;
    jboolean is_child_zygote = JNI_FALSE;
    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_samsung_m_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, debug_flags, rlimits, mount_external, se_info, category,
            accessInfo, se_name, fdsToClose, instructionSet, appDataDir);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}

// -----------------------------------------------------------------

void nativeSpecializeAppProcess_q(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jstring niceName,
        jboolean startChildZygote, jstring instructionSet, jstring appDataDir) {

    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeSpecializeAppProcess_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    ((nativeSpecializeAppProcess_q_t *) _nativeSpecializeAppProcess)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir);

    nativeSpecializeAppProcess_post(env, clazz);
}

void nativeSpecializeAppProcess_q_alternative(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jstring niceName,
        jboolean startChildZygote, jstring instructionSet, jstring appDataDir,
        jboolean isTopApp) {

    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeSpecializeAppProcess_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    ((nativeSpecializeAppProcess_q_alternative_t *) _nativeSpecializeAppProcess)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp);

    nativeSpecializeAppProcess_post(env, clazz);
}

void nativeSpecializeAppProcess_r(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jstring niceName,
        jboolean startChildZygote, jstring instructionSet, jstring appDataDir,
        jboolean isTopApp, jobjectArray pkgDataInfoList, jobjectArray whitelistedDataInfoList,
        jboolean bindMountAppDataDirs, jboolean bindMountAppStorageDirs) {

    nativeSpecializeAppProcess_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    ((nativeSpecializeAppProcess_r_t *) _nativeSpecializeAppProcess)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    nativeSpecializeAppProcess_post(env, clazz);
}

void nativeSpecializeAppProcess_r_dp3(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jstring niceName,
        jboolean startChildZygote, jstring instructionSet, jstring appDataDir,
        jboolean isTopApp, jobjectArray pkgDataInfoList, jboolean bindMountAppStorageDirs) {

    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;

    nativeSpecializeAppProcess_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    ((nativeSpecializeAppProcess_r_dp3_t *) _nativeSpecializeAppProcess)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            bindMountAppStorageDirs);

    nativeSpecializeAppProcess_post(env, clazz);
}

void nativeSpecializeAppProcess_r_dp2(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jstring niceName,
        jboolean startChildZygote, jstring instructionSet, jstring appDataDir,
        jboolean isTopApp, jobjectArray pkgDataInfoList) {

    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeSpecializeAppProcess_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    ((nativeSpecializeAppProcess_r_dp2_t *) _nativeSpecializeAppProcess)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList);

    nativeSpecializeAppProcess_post(env, clazz);
}


void nativeSpecializeAppProcess_samsung_q(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jint mountExternal, jstring seInfo, jint space, jint accessInfo,
        jstring niceName, jboolean startChildZygote, jstring instructionSet, jstring appDataDir) {

    jboolean isTopApp = JNI_FALSE;
    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeSpecializeAppProcess_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, niceName,
            startChildZygote, instructionSet, appDataDir, isTopApp, pkgDataInfoList,
            whitelistedDataInfoList, bindMountAppDataDirs, bindMountAppStorageDirs);

    ((nativeSpecializeAppProcess_samsung_t *) _nativeSpecializeAppProcess)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, mountExternal, seInfo, space,
            accessInfo, niceName, startChildZygote, instructionSet, appDataDir);

    nativeSpecializeAppProcess_post(env, clazz);
}

// -----------------------------------------------------------------

jint nativeForkSystemServer(
        JNIEnv *env, jclass clazz, uid_t uid, gid_t gid, jintArray gids, jint runtimeFlags,
        jobjectArray rlimits, jlong permittedCapabilities, jlong effectiveCapabilities) {

    nativeForkSystemServer_pre(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, permittedCapabilities,
            effectiveCapabilities);

    jint res = ((nativeForkSystemServer_t *) _nativeForkSystemServer)(
            env, clazz, uid, gid, gids, runtimeFlags, rlimits, permittedCapabilities,
            effectiveCapabilities);

    nativeForkSystemServer_post(env, clazz, res);
    return res;
}

jint nativeForkAndSpecialize_q_alternative(
        JNIEnv *env, jclass clazz, jint uid, jint gid, jintArray gids, jint runtime_flags,
        jobjectArray rlimits, jint mount_external, jstring se_info, jstring se_name,
        jintArray fdsToClose, jintArray fdsToIgnore, jboolean is_child_zygote,
        jstring instructionSet, jstring appDataDir, jboolean isTopApp) {

    jobjectArray pkgDataInfoList = nullptr;
    jobjectArray whitelistedDataInfoList = nullptr;
    jboolean bindMountAppDataDirs = JNI_FALSE;
    jboolean bindMountAppStorageDirs = JNI_FALSE;

    nativeForkAndSpecialize_pre(env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external,
                                se_info, se_name, fdsToClose, fdsToIgnore, is_child_zygote,
                                instructionSet, appDataDir, isTopApp, pkgDataInfoList, whitelistedDataInfoList,
                                bindMountAppDataDirs, bindMountAppStorageDirs);

    jint res = ((nativeForkAndSpecialize_q_alternative_t *) _nativeForkAndSpecialize)(
            env, clazz, uid, gid, gids, runtime_flags, rlimits, mount_external, se_info, se_name,
            fdsToClose, fdsToIgnore, is_child_zygote, instructionSet, appDataDir, isTopApp);

    nativeForkAndSpecialize_post(env, clazz, uid, res);
    return res;
}
static JNINativeMethod *onRegisterZygote(
        JNIEnv *env, const char *className, const JNINativeMethod *methods, int numMethods) {

    auto *newMethods = new JNINativeMethod[numMethods];
    memcpy(newMethods, methods, sizeof(JNINativeMethod) * numMethods);

    JNINativeMethod method;
    for (int i = 0; i < numMethods; ++i) {
        method = methods[i];

        if (strcmp(method.name, "nativeForkAndSpecialize") == 0) {
            set_nativeForkAndSpecialize(method.fnPtr);

            if (strcmp(nativeForkAndSpecialize_r_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_r;
            else if (strcmp(nativeForkAndSpecialize_p_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_p;
            else if (strcmp(nativeForkAndSpecialize_oreo_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_oreo;
            else if (strcmp(nativeForkAndSpecialize_marshmallow_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_marshmallow;

            else if (strcmp(nativeForkAndSpecialize_r_dp3_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_r_dp3;
            else if (strcmp(nativeForkAndSpecialize_r_dp2_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_r_dp2;

            else if (strcmp(nativeForkAndSpecialize_q_alternative_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_q_alternative;

            else if (strcmp(nativeForkAndSpecialize_samsung_p_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_samsung_p;
            else if (strcmp(nativeForkAndSpecialize_samsung_o_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_samsung_o;
            else if (strcmp(nativeForkAndSpecialize_samsung_n_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_samsung_n;
            else if (strcmp(nativeForkAndSpecialize_samsung_m_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkAndSpecialize_samsung_m;

            else
                LOGW("found nativeForkAndSpecialize but signature %s mismatch", method.signature);


        } else if (strcmp(method.name, "nativeSpecializeAppProcess") == 0) {
            set_nativeSpecializeAppProcess(method.fnPtr);

            if (strcmp(nativeSpecializeAppProcess_r_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeSpecializeAppProcess_r;
            else if (strcmp(nativeSpecializeAppProcess_q_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeSpecializeAppProcess_q;
            else if (strcmp(nativeSpecializeAppProcess_q_alternative_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeSpecializeAppProcess_q_alternative;
            else if (strcmp(nativeSpecializeAppProcess_sig_samsung_q, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeSpecializeAppProcess_samsung_q;

            else if (strcmp(nativeSpecializeAppProcess_r_dp3_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeSpecializeAppProcess_r_dp3;
            else if (strcmp(nativeSpecializeAppProcess_r_dp2_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeSpecializeAppProcess_r_dp2;

            else
                LOGW("found nativeSpecializeAppProcess but signature %s mismatch",
                     method.signature);

        } else if (strcmp(method.name, "nativeForkSystemServer") == 0) {
            set_nativeForkSystemServer(method.fnPtr);

            if (strcmp(nativeForkSystemServer_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkSystemServer;
            else if (strcmp(nativeForkSystemServer_samsung_q_sig, method.signature) == 0)
                newMethods[i].fnPtr = (void *) nativeForkSystemServer_samsung_q;
            else
                LOGW("found nativeForkSystemServer but signature %s mismatch", method.signature);


        }
    }

    return newMethods;
}


static JNINativeMethod *onRegisterSystemProperties(
        JNIEnv *env, const char *className, const JNINativeMethod *methods, int numMethods) {

    auto *newMethods = new JNINativeMethod[numMethods];
    memcpy(newMethods, methods, sizeof(JNINativeMethod) * numMethods);

    JNINativeMethod method;
    for (int i = 0; i < numMethods; ++i) {
        method = methods[i];

        if (strcmp(method.name, "native_set") == 0) {
            set_SystemProperties_set(method.fnPtr);

            if (strcmp("(Ljava/lang/String;Ljava/lang/String;)V", method.signature) == 0)
                newMethods[i].fnPtr = (void *) SystemProperties_set;
            else
                LOGW("found native_set but signature %s mismatch", method.signature);

            if (newMethods[i].fnPtr != methods[i].fnPtr) {
                LOGI("replaced android.os.SystemProperties#native_set");
            }
        }
    }
    return newMethods;
}

#define XHOOK_REGISTER(PATH_REGEX, NAME) \
    if (xhook_register(PATH_REGEX, #NAME, (void*) new_##NAME, (void **) &old_##NAME) != 0) \
        LOGE("failed to register hook " #NAME "."); \

#define NEW_FUNC_DEF(ret, func, ...) \
    static ret (*old_##func)(__VA_ARGS__); \
    static ret new_##func(__VA_ARGS__)

NEW_FUNC_DEF(int, jniRegisterNativeMethods, JNIEnv *env, const char *className,
             const JNINativeMethod *methods, int numMethods) {
    put_native_method(className, methods, numMethods);

//    LOGV("jniRegisterNativeMethods %s", className);

    JNINativeMethod *newMethods = nullptr;
    if (strcmp("com/android/internal/os/Zygote", className) == 0) {
        newMethods = onRegisterZygote(env, className, methods, numMethods);
    } else if (strcmp("android/os/SystemProperties", className) == 0) {
        newMethods = onRegisterSystemProperties(env, className, methods, numMethods);
    }

    int res = old_jniRegisterNativeMethods(env, className, newMethods ? newMethods : methods,
                                           numMethods);


    delete newMethods;
    return res;
}
void unhook_jniRegisterNativeMethods() {
    xhook_register(".*\\libandroid_runtime.so$", "jniRegisterNativeMethods",
                   (void *) old_jniRegisterNativeMethods,
                   nullptr);
    if (xhook_refresh(0) == 0) {
        xhook_clear();
    }
}
static void read_prop() {
    char sdk[PROP_VALUE_MAX + 1];
    if (__system_property_get("ro.build.version.sdk", sdk) > 0)
        sdkLevel = atoi(sdk);

    if (__system_property_get("ro.build.version.preview_sdk", sdk) > 0)
        previewSdkLevel = atoi(sdk);

    __system_property_get("ro.build.version.release", androidVersionName);

    LOGI("system version %s (api %d, preview_sdk %d)", androidVersionName, sdkLevel, previewSdkLevel);
}
ssize_t get_self_cmdline(char *cmdline, char zero_replacement) {
    int fd;
    ssize_t size;

    char buf[PATH_MAX];
    snprintf(buf, sizeof(buf), "/proc/self/cmdline");
    if (access(buf, R_OK) == -1 || (fd = open(buf, O_RDONLY)) == -1)
        return 1;

    if ((size = read(fd, cmdline, ARG_MAX)) > 0) {
        for (ssize_t i = 0; i < size - 1; ++i) {
            if (cmdline[i] == 0)
                cmdline[i] = zero_replacement;
        }
    } else {
        cmdline[0] = 0;
    }

    close(fd);
    return size;
}
extern "C" void constructor() __attribute__((constructor));
void constructor() {
    if (getuid() != 0)
        return;

    char cmdline[ARG_MAX + 1];
    get_self_cmdline(cmdline, 0);

    if (strcmp(cmdline, "zygote") != 0
        && strcmp(cmdline, "zygote32") != 0
        && strcmp(cmdline, "zygote64") != 0
        && strcmp(cmdline, "usap32") != 0
        && strcmp(cmdline, "usap64") != 0) {
        LOGW("not zygote (cmdline=%s)", cmdline);
        return;
    }


    read_prop();

    XHOOK_REGISTER(".*\\libandroid_runtime.so$", jniRegisterNativeMethods);

    if (xhook_refresh(0) == 0) {
        xhook_clear();
        LOGI("hook installed");
    } else {
        LOGE("failed to refresh hook");
    }

}
#pragma clang diagnostic pop