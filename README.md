1.与官网区别 全面支持android 8，9，10，11

 2.去掉不稳定hook

 3.支持最新的sandhook

 4.整合riru方便开发

 5.去掉whale 64 hook 和sandhook native hook

 6.项目运行流程：项目右侧-》XEdXposed->edxp-core->Tasks->other->pushSandhookRelease,zipSandhookRelease

 7.HiddenapiStubs上传github https://github.com/liumengdeqq/HiddenapiStubs

 8.删除HiddenapiStubs代码 hiddenApiStubJarFilePath这个变量不能唯空

 9.删除HiddenapiStubs随着gradle升级class目录会变化导致编译报错 现在上传github 更gradle就不会挂钩

 参考：
 1.https://github.com/ElderDrivers/EdXposed
 
 2.https://github.com/ganyao114/SandHook
 
 3.https://github.com/RikkaApps/Riru
 
 4.https://github.com/topjohnwu/Magisk